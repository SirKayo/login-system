var express = require('express');
var bodyParser = require('body-parser');
var multer = require('multer');

var app = express();
var upload = multer();
var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/my_db');
var accountSchema = mongoose.Schema({
	email:String,
   	name: String,
   	password: String
});
var Account = mongoose.model("Account", accountSchema);

app.set('view engine', 'pug');
app.set('views','./views');

app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({ extended: true })); 
app.use(upload.array()); 
app.use(express.static('public'));


app.get('/', function(req, res){
   res.render("login");
});
app.post('/', function(req, res){
   var accountInfo = req.body;

   if(!accountInfo.name || !accountInfo.password){
      res.render('login', {
         message: "Please, fill all the forms!", type: "error", 
         name: accountInfo.name,
         password: accountInfo.password});
   }else{
      Account.findOne({name: accountInfo.name}, "password", function(err,response){
         if(response.password==accountInfo.password){
            res.redirect('/logged');
         }else{
            res.render('login', { message:"Password incorrect",name: accountInfo.name})
         }
      });
   }
})

app.get('/logged', function(req, res){
   res.render("logged");
});

app.get('/registered', function(req, res){
   res.render("registered");
});

app.get('/signup', function(req, res){
   res.render("signup");
});
app.post('/signup', function(req, res){
   var accountInfo = req.body;

   if(!accountInfo.email || !accountInfo.name || !accountInfo.password){
      res.render('signup', {
         message: "Please, fill all the forms!", type: "error", 
         email: accountInfo.email, 
         name: accountInfo.name,
         password: accountInfo.password
      });
   }else{
      var newAccount = new Account({email: accountInfo.email,name: accountInfo.name,password: accountInfo.password});

      newAccount.save(function(err, Account){
         if(err){
            res.render('signup', {message: "Database error", type: "error"});
         }else{
            res.redirect('/registered');
         }
      });
   }
});
app.listen(3000);